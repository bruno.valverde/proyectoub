# frameworkprogramacion

## QuickStart

### Inicializar el servidor de MongoDB: 
* Para Ubuntu: En la terminal escribir **service mongod start**
* Para Windows: Buscar la carpeta donde está instalado mongoDB y correr el ejecutable de mongod

### Si desea cambiar el puerto
* Abrir el archivo app.js y modificar el puerto, por defecto viene en **5500**

### Ejecutar el servidor NodeJS
* Ubicarse en la carpeta **model** y ejecutar el comando **npm start**

